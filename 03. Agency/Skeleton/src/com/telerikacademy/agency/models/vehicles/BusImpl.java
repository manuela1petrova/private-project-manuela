package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.contracts.Printable;
import com.telerikacademy.agency.models.vehicles.contracts.Bus;

public class BusImpl extends VehicleBase implements Bus, Printable {


    private static final int BUS_MIN_CAPACITY = 10;
    private static final int BUS_MAX_CAPACITY = 50;
    private static final String BUS_CAPACITY_ERROR = String.format("A bus cannot have less than %d passengers or more than %d passengers.",
            BUS_MIN_CAPACITY, BUS_MAX_CAPACITY);

    public BusImpl(int passengerCapacity, double pricePerKilometer) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        this.setPassengerCapacity(passengerCapacity);
    }

    @Override
    public void setPassengerCapacity(int newPassengerCapacity) {
        if (newPassengerCapacity < BUS_MIN_CAPACITY || newPassengerCapacity > BUS_MAX_CAPACITY) {
            throw new IllegalArgumentException(BUS_CAPACITY_ERROR);
        }
        this.passengerCapacity = newPassengerCapacity;
    }

    @Override
    public String print() {
        return String.format("%s\n", super.print());
    }
}
