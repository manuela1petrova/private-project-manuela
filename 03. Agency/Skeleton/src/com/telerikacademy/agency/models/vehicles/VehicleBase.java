package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.vehicles.contracts.Vehicle;

public class VehicleBase implements Vehicle {
    private static final String TYPE_CANT_BE_NULL = "The type cannot be null.";

    private static final double MIN_PRICE_PER_KM = 0.10;
    private static final double MAX_PRICE_PER_KM = 2.50;
    private static final String PRICE_ERROR_MESSAGE = String.format("A vehicle with a price per kilometer lower than $%.2f or higher than $%.2f cannot exist!",
            MIN_PRICE_PER_KM, MAX_PRICE_PER_KM);

    private static final int ADDITIONAL_MIN_CAPACITY = 1;
    private static final int ADDITIONAL_MAX_CAPACITY = 800;
    private static final String ADDITIONAL_CAPACITY_ERROR = String.format("A vehicle with less than %d passengers or more than %d passengers cannot exist!",
            ADDITIONAL_MIN_CAPACITY, ADDITIONAL_MAX_CAPACITY);


//    private int passengerCapacity;
    protected int passengerCapacity;
    private double pricePerKilometer;
    private VehicleType type;

    public VehicleBase(int passengerCapacity, double pricePerKilometer, VehicleType type) {

        setPassengerCapacity(passengerCapacity);
        setPricePerKilometer(pricePerKilometer);
        setType(type);
    }

    public String printClassName() {
        return null;
    }

    public void setPassengerCapacity(int newPassengerCapacity) {
        if (newPassengerCapacity < ADDITIONAL_MIN_CAPACITY || newPassengerCapacity > ADDITIONAL_MAX_CAPACITY) {
            throw new IllegalArgumentException(ADDITIONAL_CAPACITY_ERROR);
        }
        this.passengerCapacity = newPassengerCapacity;
    }

    public void setPricePerKilometer(double newPricePerKilometer) {
        if (newPricePerKilometer < MIN_PRICE_PER_KM || newPricePerKilometer > MAX_PRICE_PER_KM) {
            throw new IllegalArgumentException(PRICE_ERROR_MESSAGE);
        }
        this.pricePerKilometer = newPricePerKilometer;
    }

    private void setType(VehicleType newType) {
        if (newType == null) {
            throw new IllegalArgumentException(TYPE_CANT_BE_NULL);
        }
        this.type = newType;
    }

    public String getShortName() {
        return getClass().getSimpleName().replace("Impl", "");

    }

    @Override
    public int getPassengerCapacity() {
        return this.passengerCapacity;
    }

    @Override
    public double getPricePerKilometer() {
        return this.pricePerKilometer;
    }

    @Override
    public VehicleType getType() {
        return this.type;
    }

    @Override
    public String print() {
        return String.format("%s ----\nPassenger capacity: %d\nPrice per kilometer: %.2f\nVehicle type: %s", getShortName(),
                getPassengerCapacity(), getPricePerKilometer(), getType().toString());
    }

    public String toString() {
        return print();
    }
}
