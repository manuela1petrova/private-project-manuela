package com.telerikacademy.agency.models.vehicles;

import com.telerikacademy.agency.models.common.VehicleType;
import com.telerikacademy.agency.models.contracts.Printable;
import com.telerikacademy.agency.models.vehicles.contracts.Train;

public class TrainImpl extends VehicleBase implements Train, Printable {

    private static final int MIN_CARTS_QUANTITY = 1;
    private static final int MAX_CARTS_QUANTITY = 15;
    private static final String CARTS_QUANTITY_ERROR = String.format("A train cannot have less than %d cart or more than %d carts.",
            MIN_CARTS_QUANTITY, MAX_CARTS_QUANTITY);

    private static final int TRAIN_MIN_CAPACITY = 30;
    private static final int TRAIN_MAX_CAPACITY = 150;
    private static final String TRAIN_CAPACITY_ERROR = String.format("A train cannot have less than %d passengers or more than %d passengers.",
            TRAIN_MIN_CAPACITY, TRAIN_MAX_CAPACITY);

    private int carts;

    public TrainImpl(int passengerCapacity, double pricePerKilometer, int carts) {
        super(passengerCapacity, pricePerKilometer, VehicleType.LAND);
        setPassengerCapacity(passengerCapacity);
        this.setCarts(carts);
    }

    public void setCarts(int newCarts) {
        if (newCarts < MIN_CARTS_QUANTITY || newCarts > MAX_CARTS_QUANTITY) {
            throw new IllegalArgumentException(CARTS_QUANTITY_ERROR);
        }
        this.carts = newCarts;
    }

    @Override
    public void setPassengerCapacity(int newPassengerCapacity) {
        if (newPassengerCapacity < TRAIN_MIN_CAPACITY || newPassengerCapacity > TRAIN_MAX_CAPACITY) {
            throw new IllegalArgumentException(TRAIN_CAPACITY_ERROR);
        }
        this.passengerCapacity = newPassengerCapacity;
    }

    @Override
    public int getPassengerCapacity() {
        return passengerCapacity;
    }

    @Override
    public int getCarts() {
        return carts;
    }

    @Override
    public String print() {
        return String.format("%s\nCarts amount: %d\n", super.print(), getCarts());
    }
}
