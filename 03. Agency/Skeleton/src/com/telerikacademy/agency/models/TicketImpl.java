package com.telerikacademy.agency.models;

import com.telerikacademy.agency.models.contracts.Journey;
import com.telerikacademy.agency.models.contracts.Ticket;


public class TicketImpl implements Ticket {

    private static final String ADMIN_COST_ERROR = "Administration cost cannot be under 0.";
//    private static final String JOURNEY_ERROR = "Journey cannot be null.";
    private double administrativeCosts;
    private Journey journey;

    public TicketImpl(Journey journey, double administrativeCosts) {
        this.setJourney(journey);
        this.setAdministrativeCosts(administrativeCosts);
    }

    public void setAdministrativeCosts(double newSetAdministrativeCosts) {
        if (newSetAdministrativeCosts < 0) {
            throw new IllegalArgumentException(ADMIN_COST_ERROR);
        }
        this.administrativeCosts = newSetAdministrativeCosts;
    }

    public void setJourney(Journey newJourney) {
//        if (newJourney == null) {
//            throw new IllegalArgumentException(JOURNEY_ERROR);
//        }
        this.journey = newJourney;
    }

    @Override
    public double getAdministrativeCosts() {
        return administrativeCosts;
    }

    @Override
    public Journey getJourney() {
        return journey;
    }

    @Override
    public double calculatePrice() {
        return administrativeCosts * journey.calculateTravelCosts();
    }

    @Override
    public String print() {
        return String.format("Ticket ----\nDestination: %s\nPrice: %.2f\n", journey.getDestination(), calculatePrice());
    }

    public String toString() {
        return print();
    }
}
