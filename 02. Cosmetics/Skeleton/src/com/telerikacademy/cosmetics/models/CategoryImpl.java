package com.telerikacademy.cosmetics.models;


import com.telerikacademy.cosmetics.models.contracts.Category;
import com.telerikacademy.cosmetics.models.contracts.Product;
import sun.reflect.generics.reflectiveObjects.NotImplementedException;

import java.util.ArrayList;
import java.util.List;

public class CategoryImpl implements Category {

    private static final int NAME_MIN_LENGTH = 1;
    private static final String NAME_INVALID_MESSAGE = String.format("Name should be longer than %d symbols.",
            NAME_MIN_LENGTH);
    private static final String PRODUCT_INVALID_MESSAGE = "Product cannot be null.";
    private static final String PRODUCT_EXISTS_ERROR = "This product already exists in this category.";
    private static final String PRODUCT_NOT_EXIST_ERROR = "This product does not exist.";

    //use constants for validations values
    
    private String name;
    private List<Product> products;
    
    public CategoryImpl(String name) {
        //validate
        this.setName(name);

        this.products = new ArrayList<>();
    }

    public void setName(String newName) {
        if (newName == null) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        if (newName.length() < NAME_MIN_LENGTH) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        this.name = newName;
    }

    public String getName() {
        return name;
    }
    
    public List<Product> getProducts() {
        //todo why are we returning a copy? Replace this comment with explanation!
        return new ArrayList<>(products);
    }
    
    public void addProduct(Product product) {
        if (product == null) {
            throw new IllegalArgumentException(PRODUCT_INVALID_MESSAGE);
        }
        if (products.contains(product)) {
            throw new IllegalArgumentException(PRODUCT_EXISTS_ERROR);
        }
        this.products.add(product);
    }
    
    public void removeProduct(Product product) {
        //validate
        if (product == null) {
            throw new IllegalArgumentException(PRODUCT_INVALID_MESSAGE);
        }
        if (!products.contains(product)) {
            throw new IllegalArgumentException(PRODUCT_NOT_EXIST_ERROR);
        }
        this.products.remove(product);
    }
    
    //The engine calls this method to print your category! You should not rename it!
    public String print() {
        if (products.size() == 0) {
            return String.format("#Category: %s\n" +
                    " #No product in this category", name);
        }
        StringBuilder result = new StringBuilder();
        result.append("#Category: ").append(name).append("\n");
        for (Product pr : products) {
            result.append(pr.print());
        }
        //finish ProductBase class before implementing this method
       return result.toString();
    }
    
}
