package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.UsageType;
import com.telerikacademy.cosmetics.models.contracts.Shampoo;

public class ShampooImpl extends ProductBase implements Shampoo {

    private static final String MILLILITERS_INVALID_MESSAGE = "Milliliters should be non negative.";
    private static final String USAGE_INVALID_MESSAGE = "Usage type cannot be null.";

    private int milliliters;
    private UsageType usage;


    public ShampooImpl(String name, String brand, double price, GenderType gender, int milliliters, UsageType usage) {
        super(name, brand, price, gender);

        this.setMilliliters(milliliters);
        this.setUsage(usage);

    }

    public void setMilliliters(int newMilliliters) {
        if (newMilliliters <= 0) {
            throw new IllegalArgumentException(MILLILITERS_INVALID_MESSAGE);
        }
        this.milliliters = newMilliliters;
    }

    public void setUsage(UsageType newUsage) {
        if (newUsage == null) {
            throw new IllegalArgumentException(USAGE_INVALID_MESSAGE);
        }
        this.usage = newUsage;
    }

    @Override
    public int getMilliliters() {
        return milliliters;
    }

    @Override
    public UsageType getUsage() {
        return usage;
    }

    @Override
    public String print() {
        return String.format("%s\n #Milliliters: %d\n #Usage: %s\n===",
                super.print(), getMilliliters(), getUsage().toString());

    }
}
