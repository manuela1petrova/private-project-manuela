package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.common.ScentType;
import com.telerikacademy.cosmetics.models.contracts.Cream;

public class CreamImpl extends ProductBase implements Cream {

    private static final String SCENT_INVALID_MESSAGE = "Scent type cannot be null.";
    private ScentType scent;

    public CreamImpl(String name, String brand, double price, GenderType gender, ScentType scent) {
        super(name, brand, price, gender);

        this.setScent(scent);

    }

    public void setScent(ScentType newScent) {
        if (newScent == null) {
            throw new IllegalArgumentException(SCENT_INVALID_MESSAGE);
        }
        this.scent = newScent;
    }

    @Override
    public ScentType getScent() {
        return scent;
    }

    @Override
    public String print() {
        return String.format("%s\n #Scent: %s\n===",
                super.print(), scent.toString());
    }
}
