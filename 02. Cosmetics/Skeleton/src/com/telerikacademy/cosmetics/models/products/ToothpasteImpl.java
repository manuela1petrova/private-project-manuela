package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Toothpaste;

import java.util.ArrayList;
import java.util.List;

public class ToothpasteImpl extends ProductBase implements Toothpaste {

    private static final String INGREDIENTS_INVALID_MESSAGE = "Ingredients list cannot be null.";

    private List<String> ingredients;


    public ToothpasteImpl(String name, String brand, double price, GenderType gender, List<String> ingredients) {
        super(name, brand, price, gender);

        this.setIngredients(ingredients);

    }

    public void setIngredients(List<String> newIngredients) {
        if (newIngredients == null) {
            throw new IllegalArgumentException(INGREDIENTS_INVALID_MESSAGE);
        }
        this.ingredients = newIngredients;
    }

    @Override
    public List<String> getIngredients() {
        return new ArrayList<>(ingredients);
    }

    @Override
    public String print() {
        return String.format("%s\n #Ingredients: %s\n===",
                super.print(), ingredients.toString());
    }
}
