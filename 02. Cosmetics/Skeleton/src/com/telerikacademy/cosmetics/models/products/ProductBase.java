package com.telerikacademy.cosmetics.models.products;

import com.telerikacademy.cosmetics.models.common.GenderType;
import com.telerikacademy.cosmetics.models.contracts.Product;

public class ProductBase implements Product {
    //Finish the class
    //implement proper interface (see contracts package)
    //validate

    private static final int NAME_MIN_LENGTH = 3;
    private static final int NAME_MAX_LENGTH = 10;
    private static final String NAME_INVALID_MESSAGE = String.format("Name should be between %d and %d symbols.",
            NAME_MIN_LENGTH, NAME_MAX_LENGTH);
    private static final int BRAND_MIN_LENGTH = 2;
    private static final int BRAND_MAX_LENGTH = 10;
    private static final String BRAND_INVALID_MESSAGE = String.format("Brand should be between %d and %d symbols.",
            BRAND_MIN_LENGTH, BRAND_MAX_LENGTH);
    private static final String PRICE_INVALID_MESSAGE = "Price should be non negative.";
    private static final String GENDER_INVALID_MESSAGE = "Gender type cannot be null.";

    public String name;
    private String brand;
    private double price;
    private GenderType gender;
    
    public ProductBase(String name, String brand, double price, GenderType gender) {

        this.setName(name);
        this.setBrand(brand);
        this.setPrice(price);
        this.setGender(gender);

    }

    public void setName(String newName) {
        if (newName == null) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        if (newName.length() < NAME_MIN_LENGTH || newName.length() > NAME_MAX_LENGTH) {
            throw new IllegalArgumentException(NAME_INVALID_MESSAGE);
        }
        this.name = newName;
    }

    public void setBrand(String newBrand) {
        if (newBrand == null) {
            throw new IllegalArgumentException(BRAND_INVALID_MESSAGE);
        }
        if (newBrand.length() < BRAND_MIN_LENGTH || newBrand.length() > BRAND_MAX_LENGTH) {
            throw new IllegalArgumentException(BRAND_INVALID_MESSAGE);
        }
        this.brand = newBrand;
    }

    public void setPrice(double newPrice) {
        if (newPrice <= 0) {
            throw new IllegalArgumentException(PRICE_INVALID_MESSAGE);
        }
        this.price = newPrice;
    }

    public void setGender(GenderType newGender) {
        if (newGender == null) {
            throw new IllegalArgumentException(GENDER_INVALID_MESSAGE);
        }
            this.gender = newGender;
    }

    @Override
    public String getName() {
        return this.name;
    }

    @Override
    public String getBrand() {
        return this.brand;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public GenderType getGender() {
        return this.gender;
    }

    @Override
    public String print() {
        return String.format("#%s %s\n #Price: %.2f\n #Gender: %s", getName(), getBrand(), getPrice(), getGender().toString());
    }
}
