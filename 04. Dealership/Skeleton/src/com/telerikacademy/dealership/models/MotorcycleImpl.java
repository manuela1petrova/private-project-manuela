package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Motorcycle;

public class MotorcycleImpl extends VehicleBase implements Motorcycle {

    private String category;

    public MotorcycleImpl(String make, String model, double price, String category) {
        super(make, model, price, VehicleType.MOTORCYCLE);
        setCategory(category);
    }

    private void setCategory(String category) {
        if (category.length() < ModelsConstants.MIN_CATEGORY_LENGTH || category.length() > ModelsConstants.MAX_CATEGORY_LENGTH) {
            throw new IllegalArgumentException(String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX, "Category",
                    ModelsConstants.MIN_CATEGORY_LENGTH, ModelsConstants.MAX_CATEGORY_LENGTH));
        }
        this.category = category;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Category: %s", category);

    }

    @Override
    public String getCategory() {
        return category;
    }
}
