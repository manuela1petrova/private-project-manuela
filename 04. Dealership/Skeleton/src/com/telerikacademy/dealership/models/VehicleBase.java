package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.Utils;
import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Comment;
import com.telerikacademy.dealership.models.contracts.Vehicle;

import java.util.ArrayList;
import java.util.List;

public abstract class VehicleBase implements Vehicle {
    
    private final static String MAKE_FIELD = "Make";
    private final static String MODEL_FIELD = "Model";
    private final static String PRICE_FIELD = "Price";
    private final static String WHEELS_FIELD = "Wheels";
    private final static String COMMENTS_HEADER = "    --COMMENTS--";
    private final static String NO_COMMENTS_HEADER = "    --NO COMMENTS--";

    //add fields
    private List<Comment> comments = new ArrayList<>();
    private String make;
    private String model;
    private double price;
    private VehicleType vehicleType;



    // finish the constructor and validate input; look in package com.telerikacademy.dealership.models.common.enums; what methods are there in VehicleType?
    public VehicleBase(String make, String model, double price, VehicleType vehicleType) {
        setMake(make);
        setModel(model);
        setPrice(price);
        setType(vehicleType);
    }


    private void setMake(String newMake) {
        if (newMake.length() < ModelsConstants.MIN_MAKE_LENGTH || newMake.length() > ModelsConstants.MAX_MAKE_LENGTH) {
            throw new IllegalArgumentException(String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX, "Make",
                    ModelsConstants.MIN_MAKE_LENGTH, ModelsConstants.MAX_MAKE_LENGTH));
        }

        make = newMake;
    }

    private void setModel(String newModel) {
        if (newModel.length() < ModelsConstants.MIN_MODEL_LENGTH || newModel.length() > ModelsConstants.MAX_MODEL_LENGTH) {
            throw new IllegalArgumentException(String.format(ModelsConstants.STRING_MUST_BE_BETWEEN_MIN_AND_MAX, "Model",
                    ModelsConstants.MIN_MODEL_LENGTH, ModelsConstants.MAX_MODEL_LENGTH));
        }
        model = newModel;
    }

    private void setPrice(double newPrice) {
        if (newPrice < ModelsConstants.MIN_PRICE || newPrice > ModelsConstants.MAX_PRICE) {
            throw new IllegalArgumentException(String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, "Price",
                    ModelsConstants.MIN_PRICE, ModelsConstants.MAX_PRICE));
        }
        price = newPrice;
    }

    private void setType(VehicleType vehicleType) {
        this.vehicleType = vehicleType;
    }

    public void setComments(List<Comment> comments) {
        if (comments == null) {
            throw new IllegalArgumentException(ModelsConstants.COMMENT_CANNOT_BE_NULL);
        }
        this.comments = comments;
    }

    @Override
    public int getWheels() {
        return vehicleType.getWheelsFromType();
    }

    @Override
    public VehicleType getType() {
        return this.vehicleType;
    }

    @Override
    public String getMake() {
        return this.make;
    }

    @Override
    public String getModel() {
        return this.model;
    }

    @Override
    public double getPrice() {
        return this.price;
    }

    @Override
    public List<Comment> getComments() {
        return new ArrayList<>(comments);
    }

    @Override
    public void removeComment(Comment comment) {
        comments.remove(comment);
    }

    public void addComment(Comment comment) {
        comments.add(comment);
    }

    @Override
    public String toString() {
        StringBuilder builder = new StringBuilder();
        
        builder.append(String.format("%s:", this.getClass().getSimpleName().replace("Impl", ""))).append(System.lineSeparator());
        //finish implementation of toString() - what should the next 3 lines of code append to the builder?
        builder.append(String.format("  %s: %s", MAKE_FIELD, getMake())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", MODEL_FIELD, getModel())).append(System.lineSeparator());
        builder.append(String.format("  %s: %s", WHEELS_FIELD, getWheels())).append(System.lineSeparator());
        
        builder.append(String.format("  %s: $%s", PRICE_FIELD, Utils.removeTrailingZerosFromDouble(price))).append(System.lineSeparator());
        
        if (!printAdditionalInfo().isEmpty()) {
            builder.append(printAdditionalInfo()).append(System.lineSeparator());
        }
        builder.append(printComments());
        return builder.toString();
    }
    
    //todo replace this comment with explanation why this method is protected: The string can't be accessed in other package but this one.
    protected abstract String printAdditionalInfo();
    
    private String printComments() {
        StringBuilder builder = new StringBuilder();
        
        if (comments.size() <= 0) {
            builder.append(String.format("%s", NO_COMMENTS_HEADER));
        } else {
            builder.append(String.format("%s", COMMENTS_HEADER)).append(System.lineSeparator());
            
            for (Comment comment : comments) {
                builder.append(comment.toString());
            }
            
            builder.append(String.format("%s", COMMENTS_HEADER));
        }
        
        return builder.toString();
    }
}
