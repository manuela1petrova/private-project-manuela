package com.telerikacademy.dealership.models;

import com.telerikacademy.dealership.models.common.enums.VehicleType;
import com.telerikacademy.dealership.models.contracts.Truck;

public class TruckImpl extends VehicleBase implements Truck {

    private int weightCapacity;

    public TruckImpl(String make, String model, double price, int weightCapacity) {
        super(make, model, price, VehicleType.TRUCK);
        setWeightCapacity(weightCapacity);
    }

    private void setWeightCapacity(int weightCapacity) {
        if (weightCapacity < ModelsConstants.MIN_CAPACITY || weightCapacity > ModelsConstants.MAX_CAPACITY) {
            throw new IllegalArgumentException(String.format(ModelsConstants.NUMBER_MUST_BE_BETWEEN_MIN_AND_MAX, "Weight capacity",
                    ModelsConstants.MIN_CAPACITY, ModelsConstants.MAX_CAPACITY));
        }
        this.weightCapacity = weightCapacity;
    }

    @Override
    protected String printAdditionalInfo() {
        return String.format("  Weight Capacity: %dt", getWeightCapacity());
    }

    @Override
    public int getWeightCapacity() {
        return weightCapacity;
    }
    
}
