# Test Design Techniques - Homework

## Requirement: _Generate test ideas based on the Test Design Techniques about forum topics. Focus on topic creation, commenting and notifications. Make sure to add priority and to  mention which technique you used for each test case._


|№|Test Case Title|Priority|Technique|
|---|---|---|---|
|1.|Check login with correct credentials|Prio 1|Pairwise testing|
|2.|Check if the account is temporarily banned after unsuccessfull login 3 times|Prio 3|Use case testing|
|3.|Check if unregistered user can create new topic|Prio 1|Decision table|
|4.|Check if we can create a topic with empty title|Prio 1|Pairwise testing|
|5.|Check if the length of the tite is between 5 and 15 symbols|Prio 2|BVA|
|6.|Check if we can post a new topic without text in the comment field|Prio 1|Pairwise testing|
|7.|Check if the length of the comment is from 5 to 300 symbols|Prio 2|BVA|
|8.|Check if Bold, Italics and Underline formatting work together|Prio 3|Pairwise testing|
|9.|Check if we can include hyperlink in the text|Prio 2|Pairwise testing|
|10.|Check if creation of a new topic is allowed for unregistered users|Prio 1|Eror guessing|
|11.|Check if a file can be attached in the comment field|Prio 3|Pairwise testing|
|12.|Check if the size of the attached file is up to 3 MB|Prio 2|BVA|
|13.|Check if the new topic is visible for everyone in the forum|Prio 1|Error guessing|
|14.|Check if unregistered users can like the new topic|Prio 3|Error guessing|