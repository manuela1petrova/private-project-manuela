**Test case:** Create new topic

**Description:** A new topic should be created successfully

**Preconditions:**
1. The user must have an email address and password that is previously registered.
2. Browser cookies should be deleted before the test execution.


**Setup:**
1. OS: macOS  Big Sur v11.3
2. Used browser: Google Chrome Version 90.0.4430.212